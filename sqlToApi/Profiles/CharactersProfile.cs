﻿using AutoMapper;
using sqlToApi.Models;
using sqlToApi.Models.DTO.Characters;

namespace sqlToApi.Profiles
{
    public class CharactersProfile:Profile
    {
        public CharactersProfile()
        {
            CreateMap<Characters, ReadCharactersDTO>();
            CreateMap<Characters, CreateCharactersDTO>();
            CreateMap<CreateCharactersDTO, Characters>();
            CreateMap<ReadCharactersDTO, Characters>();
        }
    }
    // fan va gött dö
}
