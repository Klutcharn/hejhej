﻿using AutoMapper;
using sqlToApi.Models;
using sqlToApi.Models.DTO;
using sqlToApi.Models.DTO.Franchise;

namespace sqlToApi.Profiles
{
    public class FranchiseProfile: Profile
    {



        public FranchiseProfile()
        {
            CreateMap<Franchise, ReadFranchiseDTO>();
            CreateMap<Franchise, CreateFranchiseDTO>();
            CreateMap<CreateFranchiseDTO, Franchise>();
            CreateMap<ReadFranchiseDTO, Franchise>();
        }

    }
}
