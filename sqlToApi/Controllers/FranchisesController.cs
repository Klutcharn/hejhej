﻿#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using sqlToApi.Data;
using sqlToApi.Models;
using sqlToApi.Models.DTO;
using sqlToApi.Models.DTO.Franchise;

namespace sqlToApi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class FranchisesController : ControllerBase
    {
        private readonly MoviesContext _context;
        private readonly IMapper _mapper;

        public FranchisesController(MoviesContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Franchises
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadFranchiseDTO>>> GetFranchises()
        {
            return _mapper.Map<List< ReadFranchiseDTO>>(  await _context.Franchises.ToArrayAsync());
        }

        // GET: api/Franchises/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ReadFranchiseDTO>> GetFranchise(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);

            if (franchise == null)
            {
                return NotFound();
            }

            return _mapper.Map< ReadFranchiseDTO> (franchise);
        }




        //USE MAPPER
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, ReadFranchiseDTO dtoFranchise)
        {
            if (id != dtoFranchise.Id)
            {
                return BadRequest();
            }
            Franchise domainFranchise = _mapper.Map<Franchise>(dtoFranchise);

            _context.Entry(domainFranchise).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }


        //USE MAPPER
        [HttpPost]
        public async Task<ActionResult<Franchise>> PostFranchise( CreateFranchiseDTO dtoFranchise)
        {
            Franchise franchiseDomain = _mapper.Map<Franchise>(dtoFranchise);
            _context.Franchises.Add(franchiseDomain);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFranchise",
                new { id = franchiseDomain.Id },
                _mapper.Map<CreateFranchiseDTO>(franchiseDomain));
        }


        // DELETE: api/Franchises/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }

            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(e => e.Id == id);
        }
    }
}
