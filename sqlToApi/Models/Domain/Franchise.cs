﻿using System.ComponentModel.DataAnnotations;

namespace sqlToApi.Models
{
    public class Franchise
    {
        
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }


    }
}
