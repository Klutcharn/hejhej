﻿using System.ComponentModel.DataAnnotations;

namespace sqlToApi.Models
{
    public class Characters
    {
        
        public int Id {get; set; }

        public string Name { get; set; }

        public string alias { get; set; }

        public string gender { get; set; }

        public string Picture { get; set; }

        public ICollection<Movie> Movies { get; set; }
    }
}
